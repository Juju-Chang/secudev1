<!DOCTYPE html>
<%@ page isELIgnored="false" %>
<%@ page import="constants.SearchNames" %>
<%@ page import="constants.UserColumnNames" %>
<%@ page import="constants.MessageColumnNames" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>


<head>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/search.css">
	<link rel="stylesheet" type="text/css" href="css/msgBoard.css">
	<script src= "script/jquery-2.1.3.min.js"></script>
	<script src= "script/search2.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Search</title>
</head>
<body>
<div class="top">
		<div class="navBar">
			<ul>
				<li class="navItem left"><a href="index.jsp">Home</a></li>
				<li class="navItem left"><a href="AllMessages/0">Message Board</a></li>
				<li class="navItem left focus"><a href="#">Search</a></li>
				<c:if test="${user.isAdmin()}">
					<li class="navItem left"><a href="backup.jsp">Backup Messages</a></li>
				</c:if>
				<li class="navItem right"><a href="Logout">Logout</a></li>
			</ul>	
		</div>
	</div>

	<div class="sideBar left darkBlue">
	<form action="Search" method="get">
		<input type="text" class="searchText" placeholder="Search" id="searchText" name="${SearchNames.QUERY.toString()}">
		<input type="submit" value="Search">
	</form>
	<p class="filterLabel">Filter</p>
		<div id="filterContainer" class="filterContainer">

		</div>

	<button type="button" onclick="addFilter()">Add</button>
	</div>
	<div class="whole">
		<div class="overlay" onclick="removeOverlay()">
		</div>
		<%-- <center> --%>
		<div class="popupBox">
			<div class="filterBar">
				<div class="filterOption focus" onclick="username(this)">
					<p class="filterTitle">Username</p>
				</div>
				<div class="filterOption" onclick="date(this)">
					<p class="filterTitle">Date</p>
				</div>
			</div>

			<div class="filterCont">
				<div class="usernameCont">
					<p class="popLabel">Search Criteria:</p>
					<select id="userAO">
						<option value="and">And</option>
						<option value="or">Or</option>
					</select>
					<input type="text" placeholder="Username" class="regText" id="userUser">
					<button type="button" onclick="addUserFilter()">Add Filter</button>
				</div>
				<div class="dateCont">
					<p class="popLabel">Search Criteria:</p>
					<select id="dateAO">
						<option value="and">And</option>
						<option value="or">Or</option>
					</select>
					<p class="popLabel">Condition:</p>
					<select id="dateCond" >
						<option value=">="> &gt;&#61;  	</option> 
						<option value="<="> &lt;&#61;  	</option> 
						<option value="=" selected> &#61; 		</option>   
					</select>
					<p class="popLabel">Date:</p><br>
					<select id="dateYear" class="year" 	onchange="dateChanged(this)"></select>
					<select id="dateMonth" class="month" onchange="dateChanged(this)"></select>
					<select id="dateDay"  class="day"></select>

					<button type="button" onclick="addDateFilter()">Add Filter</button>
				</div>
			</div>
		</div>
		<%-- </center> --%>
	</div>
	<div class="resultsDiv right">
		<c:forEach items="${searchResults}" var="message">
			<div class="msgWrapper">
				<div class="msgContent">
					<a href="http://localhost:8080/Secudev1/User/${message.get(MessageColumnNames.USERNAME) }" class="userLink left"><c:out value="${message.get(MessageColumnNames.FIRST_NAME) }"/></a>
					<p class="username left">@<c:out value="${message.get(MessageColumnNames.USERNAME) }"/></p>
					<p class="date right">
					<c:out value="${message.get(MessageColumnNames.DATE_POSTED) }"/>
					<c:if test="${message.get(MessageColumnNames.DATE_EDITED) != null }">
						 - Edited <c:out value="${message.get(MessageColumnNames.DATE_EDITED) }"/>
					</c:if>
					</p><br>
					<p class="msgBody"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY)}" escapeXml="false"/></p>
 					
				</div>
			</div>
		</c:forEach>
		<c:remove var="searchResults"  />
		<c:remove var="filterList"  />
	</div>
</body>
</html>