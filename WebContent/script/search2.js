$(document).ready(function(){
	$(".overlay").parent().hide();
	$(".overlay").parent().children().hide();	
	$(".dateCont").children().hide();
	for (i = new Date().getFullYear(); i > 1900; i--)
		$('.year').append($('<option />').val(i).html(i));
	for (i = 1; i < 13; i++)
		$('.month').append($('<option />').val(i).html(i));
	for (i = 1; i < 32; i++)
		$(".day").append($('<option />').val(i).html(i));
});

function dateChanged(origin){
	var year  = $('.year option:selected').text();
	var month = $('.month option:selected').text();
	var day   = $(".day option:selected").text();
	console.log(year+" "+month+" "+day);
	
	$(".day option").remove();
	for(var i = 1; i <= daysInMonth(month, year); i++)	
		$(".day").append($('<option />').val(i).html(i));
	if(day <= daysInMonth(month, year)){
//		$(origin).closest(".day").val(300);
		$(origin).closest(".day").val(day);
	}
	else{
//		$(origin).closest(".day").val(300);
	}
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

function addFilter(){
	$(".overlay").parent().show();
	$(".overlay").parent().children().show();
}

function username(src){
	$(src).toggleClass("focus");
	$(src).next('.filterOption').toggleClass("focus");
	$('.dateCont').hide();
	$('.dateCont').children().hide();
	$(".usernameCont").show();
	$(".usernameCont").children().show();
}

function date(src){

	$(src).toggleClass("focus");
	$(src).prev('.filterOption').toggleClass("focus");
	$('.usernameCont').hide();
	$('.usernameCont').children().hide();
	$(".dateCont").show();
	$(".dateCont").children().show();
}

function removeOverlay(){
	$(".overlay").parent().hide();
	$(".overlay").parent().children().hide();		
}

function addUserFilter(){
	var ao = $("#userAO").val();
	var user = $("#userUser").val();
	
	var xmlhttp;    
	if (window.XMLHttpRequest)// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	else // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
		  if(xmlhttp.responseText != ""){
			  var response = xmlhttp.responseText
			  if(response == "true"){
				  $("#filterContainer").append("<div class='filter "+ ao +"'><p class='filterText'>"+user+"</p><a href='#' onclick='removeFilter(this)'></a></div>");
			  }
		  }
	  }
	}
	xmlhttp.open("GET","Search?ao=" +ao + "&username="+ user,true);
	xmlhttp.send();
	removeOverlay();
}

function addDateFilter(){
	var ao = $("#userAO").val();
	var dateCond = $("#dateCond").val();
	var dateString = $("#dateYear").val()+"/"+ $("#dateMonth").val()+"/"+ $("#dateDay").val();
	
	var xmlhttp;    
	if (window.XMLHttpRequest)// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	else // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
		  if(xmlhttp.responseText != ""){
			  var response = xmlhttp.responseText
			  if(response == "true"){
				  $("#filterContainer").append("<div class='filter "+ ao +"'><p class='filterText'>"+dateCond+" " + dateString+"</p><a href='#' onclick='removeFilter(this)'></a></div>");
			  }
		  }
	  }
	}
	xmlhttp.open("GET","Search?ao=" +ao + "&dateCond="+ dateCond + "&dateYear="+$("#dateYear").val()+"&dateMonth="+$("#dateMonth").val()+"&dateDay="+$("#dateDay").val(),true);
	xmlhttp.send();
	removeOverlay();
}

function removeFilter(src){
	var cont = $(src).parent();
	var ind = $(src).parent().index();
	$.post("Search", {del: ind}, function(result){
    });
	$(src).parent().remove();
}
