
function delFilter(src){
	$(src).parent().remove();
}

$(document).ready(function(){
	for (i = new Date().getFullYear(); i > 1900; i--)
		$('.year').append($('<option />').val(i).html(i));
	for (i = 1; i < 13; i++)
		$('.month').append($('<option />').val(i).html(i));
	for (i = 1; i < 32; i++)
		$(".day").append($('<option />').val(i).html(i));
});

function loadDate(){
	for (i = new Date().getFullYear(); i > 1900; i--)
		$(".filterContainer").last().find('.year').append($('<option />').val(i).html(i));
	for (i = 1; i < 13; i++)
		$(".filterContainer").last().find('.month').append($('<option />').val(i).html(i));
	for (i = 1; i < 32; i++)
		$(".filterContainer").last().find(".day").append($('<option />').val(i).html(i));
}

function dateChanged(origin){
	origin = $(origin).parent();
	var year  = $(origin).children('.year').val();
	var month = $(origin).children('.month').val();
	var day   = $(origin).children(".day").val();
	console.log(year+" "+month+" "+day);
	
	$(origin).children(".day").empty();
	for(var i = 1; i <= daysInMonth(month, year); i++)	
		$(origin).children(".day").append($('<option />').val(i).html(i));
	if(day <= daysInMonth(month, year)){
//		$(origin).closest(".day").val(300);
		$(origin).children(".day").val(day);
//		$(origin).sibling(".day").val(day);
	}
	else{
//		$(origin).closest(".day").val(300);
		$(origin).children(".day").val(daysInMonth(month, year));
//		$(origin).sibling(".day").val(daysInMonth(month, year));
	}
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}