<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/main.css">
<title>Backup</title>
</head>
<body>

	<c:if test="${!user.isAdmin()}">
		<c:if test="${flashMessage!=null }">
			<font color="red"><b>Error: <c:out value="${flashMessage}"/></b></font>
		</c:if>
		<c:redirect url="index.jsp"></c:redirect>
	</c:if>
	
	<c:if test="${user.isAdmin()}">
		<div class="top">
			<div class="navBar">
				<ul>
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/index.jsp">Home</a></li>
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/AllMessages/0">Message Board</a></li>
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/search.jsp">Search</a></li>
					<li class="navItem left focus"><a href="#">Backup Messages</a></li>
					<li class="navItem right"><a href="http://localhost:8080/Secudev1/Logout">Logout</a></li>
				</ul>	
			</div>
		</div>
		
		<form action="Backup" method="POST">
			<input type="text" placeholder="filename.csv"  name="fileName">
			<input type="submit" value="Create new backup"/>
		</form>
		
		<c:forEach items="${allBackups}" var="backup" varStatus ="loop">
			Backup Version # ${(loop.index+1) } : <a href="Download?filename=<c:out value="${backup }"/>"><c:out value="${backup }"/></a><br />
		</c:forEach>
	</c:if>

</body>
</html>