<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="constants.UserColumnNames" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta charset="ISO-8859-1">
</head>
<body>
	<h3>Name</h3>
	<input type="text" placeholder="First Name" name="fName" id="fName">
	<input type="text" placeholder="Last Name"  name="lName" id="lName">
	
	<h3>Gender</h3>
	<input type="radio" name="gender" onclick="setGender(this)" value="Male" checked> Male
	<input type="radio" name="gender" onclick="setGender(this)" value="Female"> Female <br>
	
	<h3>Salutation</h3>
	<select id="salutation" name="salutation">
	</select><br>
	
	<h3>Birth Date</h3>
	<select name="year"  id="year"  onchange="dateChanged(this)"></select>
	<select name="month" id="month" onchange="dateChanged(this)"></select>
	<select name="day" id="day"></select>
	<br />
</body>
</html>