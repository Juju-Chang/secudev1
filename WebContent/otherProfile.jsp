<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="constants.UserColumnNames" %>
<%@ page import="constants.MessageColumnNames" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="http://localhost:8080/Secudev1/css/main.css">
	<link rel="stylesheet" type="text/css" href="http://localhost:8080/Secudev1/css/index.css">
	<title><c:out value="${profile.get(UserColumnNames.USERNAME)}"/></title>
</head>
<body>
	<!-- Profile is: <c:out value="${profile}"/>! -->
	<c:if test="${profile!=null }">
	<div class="top">
			<div class="navBar">
				<ul>
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/index.jsp">Home</a></li>
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/AllMessages/0">Message Board</a></li>
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/search.jsp">Search</a></li>
					<c:if test="${user.isAdmin()}">
						<li class="navItem left"><a href="http://localhost:8080/Secudev1/backup.jsp">Backup Messages</a></li>
					</c:if>
					<li class="navItem right"><a href="http://localhost:8080/Secudev1/Logout">Logout</a></li>
					
				</ul>	
			</div>
		</div>
		
		<div class="profDiv">
			<div class="profWrapper">
				<div class="profImg"></div>
				<div class="profContent">
					<p class="salutation"><c:out value="${profile.get(UserColumnNames.SALUTATION)}"/></p>
					<p class="fullname"><c:out value="${profile.get(UserColumnNames.FIRST_NAME)} ${profile.get(UserColumnNames.LAST_NAME)}"/></p><br>
					<p class="username">@<c:out value="${profile.get(UserColumnNames.USERNAME)}"/></p><br><br>
					<p class="birthday right"><c:out value="${profile.get(UserColumnNames.BIRTHDATE)}"/></p>
					<p class="sex">
					<c:if test="${profile.get(UserColumnNames.IS_FEMALE) == 0 }">
						MALE
					</c:if>
					<c:if test="${profile.get(UserColumnNames.IS_FEMALE) == 1 }">
						FEMALE
					</c:if>
					</p>
					<p class="about"><c:out value="${profile.get(UserColumnNames.ABOUT_ME)}"/></p> 
				</div>
			</div>
		</div>
	
		<div class="msgDiv">
			<c:forEach items="${profileMessages }" var="message">
				<div class="msgWrapper">
					<div class="msgContent">
						<a href="http://localhost:8080/Secudev1/User/${message.get(MessageColumnNames.USERNAME) }" class="userLink"><c:out value="${message.get(MessageColumnNames.FIRST_NAME) }"/></a>
						<p class="username">@<c:out value="${message.get(MessageColumnNames.USERNAME) }"/></p>
						<p class="date right">
						<c:out value="${message.get(MessageColumnNames.DATE_POSTED) }"/>
						<c:if test="${message.get(MessageColumnNames.DATE_EDITED) != null }">
							- Edited <c:out value="${message.get(MessageColumnNames.DATE_EDITED) }"/>
						</c:if>
						</p>
						<p class="msgBody"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY)}" escapeXml="false"/></p>
					</div>
				</div>
			</c:forEach>
		</div>
	</c:if>
</body>
</html>