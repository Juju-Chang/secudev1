<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/login.css">
<script src= "script/jquery-2.1.3.min.js"></script>
<title>Login</title>
</head>

<script>
$(document).ready(function(){
	$('input').on("focus", function() {
        $(this).closest('.tile').addClass('shadowed');
        $(this).prev('.label').addClass('labelFocus');
    });
    $("input").on("blur",function() {
        $(this).closest('.tile').removeClass('shadowed');
        $(this).prev('.label').removeClass('labelFocus');
    });

});
</script>

<body>
<c:if test="${user!=null }">
	<c:redirect url="index.jsp"></c:redirect>
</c:if>

<c:if test="${user==null }">
	<c:if test="${flashMessage!=null }">
		<font color="red"><b>Error: <c:out value="${flashMessage}"/></b></font>
	</c:if>
</c:if>

<div class="tile darkBlue">
	<p class="nameLabel">Login</p><br><br>
	<form action="login" method="post">
		<p class="label">Username</p>
		<input type="text" name="username" placeholder="Username" class="regText">
		<p class="label">Password</p>
		<input type="password" name="password" placeholder="Password" class="regText"><br>
		<input type="submit" value="Login" class="regSubmit">
	</form>
	<p class="regLink">Don't have an account? </p><a href="registration.jsp">Sign up here!</a>
</div>

</body>
</html>