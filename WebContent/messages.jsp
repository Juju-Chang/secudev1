<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="constants.UserColumnNames" %>
<%@ page import="constants.MessageColumnNames" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="http://localhost:8080/Secudev1/css/main.css">
<link rel="stylesheet" type="text/css" href="http://localhost:8080/Secudev1/css/msgBoard.css">
<script src= "http://localhost:8080/Secudev1/script/jquery-2.1.3.min.js"></script>
<script src= "http://localhost:8080/Secudev1/script/message.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Message Board</title>
</head>
<body>
<div>
	<div class="top">
		<div class="navBar">
			<ul>
				<li class="navItem left"><a href="http://localhost:8080/Secudev1/index.jsp">Home</a></li>
				<li class="navItem left focus"><a href="#">Message Board</a></li>
				<li class="navItem left"><a href="http://localhost:8080/Secudev1/search.jsp">Search</a></li>
				<c:if test="${user.isAdmin()}">
					<li class="navItem left"><a href="http://localhost:8080/Secudev1/backup.jsp">Backup Messages</a></li>
				</c:if>
				<li class="navItem right"><a href="http://localhost:8080/Secudev1/Logout">Logout</a></li>
			</ul>	
		</div>
	</div>
	<c:if test="${user!=null}">
		<div class="postDiv">
			<div class="postWrapper">
				<div class="profImg"></div>
				<div class="profContent">
					<p class="fullname"><c:out value="${user.get(UserColumnNames.FIRST_NAME)} ${user.get(UserColumnNames.LAST_NAME) }"/></p><br>
					<p class="username">@<c:out value="${user.get(UserColumnNames.USERNAME) }"/></p><br><br>
					<form action="http://localhost:8080/Secudev1/Message" method="post">
						<textarea name="body" class="textContent" placeholder="Type stuff here"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY)}"/></textarea><br>
						<input type="hidden" name="msgId" value="${message.get(MessageColumnNames.MESSAGE_ID) }">
						<input type="submit" class="textSubmit" value="Post!">
					</form>
				</div>
			</div>
		</div>
	</c:if>

	<div class="msgDiv">
		<c:forEach items="${allMessages }" begin ="0" end="9" var="message">
			<div class="msgWrapper">
				<div class="msgContent">
					<a href="http://localhost:8080/Secudev1/User/${message.get(MessageColumnNames.USERNAME) }" class="userLink left"><c:out value="${message.get(MessageColumnNames.FIRST_NAME) }"/></a>
					<p class="username left">@<c:out value="${message.get(MessageColumnNames.USERNAME) }"/></p>
					<p class="date right">
					<c:out value="${message.get(MessageColumnNames.DATE_POSTED) }"/>
					<c:if test="${message.get(MessageColumnNames.DATE_EDITED) != null }">
						 - Edited <c:out value="${message.get(MessageColumnNames.DATE_EDITED) }"/>
					</c:if>
					</p><br>
					<p class="msgBody"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY)}" escapeXml="false"/></p>
					<c:if test="${user.isAdmin() || user.get(UserColumnNames.USERNAME) == message.get(MessageColumnNames.USERNAME)}">
						<form action="http://localhost:8080/Secudev1/EditMessage" method="post">
							<textarea class="edit-info textContent" name="msgEdit" placeholder="Type stuff here" cols="30" rows="5"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY)}"/></textarea><br>
							<input type="hidden" name="msgId" value="${message.get(MessageColumnNames.MESSAGE_ID) }">
							<input class="edit-info" type="submit" value="Submit Message"> 
						</form>
						<a class="edit" href="#">Edit</a> 
						<a href="http://localhost:8080/Secudev1/DeleteMessage/<c:out value="${message.get(MessageColumnNames.MESSAGE_ID)}"/>" class="delete">Delete</a>
					</c:if>
				</div>
			</div>
		</c:forEach>
		<div class="pageNumbers">
			<c:forEach begin="0" end = "${numOfPages - 1}" varStatus="loop">
				<a href="http://localhost:8080/Secudev1/AllMessages/${loop.index }">${loop.index + 1}</a>
			</c:forEach>
		</div>
	</div>
	
</div>
</body>
</html>