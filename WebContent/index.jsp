<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="constants.UserColumnNames" %>
<%@ page import="constants.MessageColumnNames" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<title>HOME</title>
</head>
<body>
	<body>
	
	<c:if test="${user==null }">
		<c:if test="${flashMessage!=null }">
			<font color="red"><b>Error: <c:out value="${flashMessage}"/></b></font>
		</c:if>
		<c:redirect url="login.jsp"></c:redirect>
	</c:if>
	
	<c:if test="${user!=null }">
		<div class="top">
			<div class="navBar">
				<ul>
					<li class="navItem left focus"><a href="#">Home</a></li>
					<li class="navItem left"><a href="AllMessages/0">Message Board</a></li>
					<li class="navItem left"><a href="search.jsp">Search</a></li>
					<c:if test="${user.isAdmin()}">
						<li class="navItem left"><a href="backup.jsp">Backup Messages</a></li>
					</c:if>
					<li class="navItem right"><a href="Logout">Logout</a></li>
					<c:if test="${user.isAdmin()}">
						<li class="navItem right"><a href="registration.jsp">Register</a></li>
					</c:if>
					<li class="navItem right"><a href="edit.jsp">Edit</a></li>
				</ul>	
			</div>
		</div>
		
		<div class="profDiv">
			<div class="profWrapper">
				<div class="profImg"></div>
				<div class="profContent">
					<p class="salutation"><c:out value="${user.get(UserColumnNames.SALUTATION)}"/></p>
					<p class="fullname"><c:out value="${user.get(UserColumnNames.FIRST_NAME)} ${user.get(UserColumnNames.LAST_NAME)}"/></p><br>
					<p class="username">@<c:out value="${user.get(UserColumnNames.USERNAME)}"/></p><br><br>
					<p class="birthday right"><c:out value="${user.get(UserColumnNames.BIRTHDATE)}"/></p>
					<p class="sex">
					<c:if test="${user.get(UserColumnNames.IS_FEMALE) == 0 }">
						MALE
					</c:if>
					<c:if test="${user.get(UserColumnNames.IS_FEMALE) == 1 }">
						FEMALE
					</c:if>
					</p>
					<p class="about"><c:out value="${user.get(UserColumnNames.ABOUT_ME)}"/></p> 
				</div>
			</div>
		</div>
	
		<div class="msgDiv">
			<c:forEach items="${userMessages }" var="message">
				<div class="msgWrapper">
					<div class="msgContent">
						<c:out value="${message.get(MessageColumnNames.FIRST_NAME) }"/>
						<p class="username">@<c:out value="${message.get(MessageColumnNames.USERNAME) }"/></p>
						<p class="date right">
							<c:out value="${message.get(MessageColumnNames.DATE_POSTED) }"/>
							<c:if test="${message.get(MessageColumnNames.DATE_EDITED) != null }">
								- Edited <c:out value="${message.get(MessageColumnNames.DATE_EDITED) }"/>
							</c:if>
						</p>
						<br>
						<p class="msgBody"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY)}" escapeXml="false"/></p>
					</div>
				</div>
			</c:forEach>
		</div>
	</c:if>
</body>
</html>