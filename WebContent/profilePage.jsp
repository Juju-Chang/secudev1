<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="constants.UserColumnNames" %>
<%@ page import="constants.MessageColumnNames" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/registration.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index</title>
</head>
<body>
	<c:if test="${user==null }">
		<c:if test="${flashMessage!=null }">
			<font color="red"><b>Error: <c:out value="${flashMessage}"/></b></font>
		</c:if>
		<c:redirect url="login.jsp"></c:redirect>
	</c:if>
	
	<c:if test="${user!=null }">
		<div class="buttCont">
			<a href="Logout" class="action-button shadow animate red">Logout</a>
		</div>
		
		<div class="main">
			<div class="header">
				<h1 class="salute">majesty</h1>
			</div>
			
			<div class="circle">
			</div>
		
			<div class="body">
				<div class="profile">
					<p class="username"><c:out 	value="${profile.get(UserColumnNames.USERNAME)}"/></p>
					<p class="name"><c:out 		value="${profile.get(UserColumnNames.SALUTATION)} ${user.get(UserColumnNames.FIRST_NAME)} ${user.get(UserColumnNames.LAST_NAME) }"/></p>
					<p class="birthday"><c:out 	value="${profile.get(UserColumnNames.BIRTHDATE)}"/></p>	
					<p class="about"><c:out 	value="${profile.get(UserColumnNames.ABOUT_ME)}"/></p>
				</div>
			</div>
		</div>	
		<c:forEach items="${profileMessages }" var="message">
			<div class="message">
				<p class="userlink"><a href="User/${message.get(MessageColumnNames.USERNAME) }">
					<c:out value="${message.get(MessageColumnNames.USERNAME) }"/></a></p>
				<p class="userlink"><a href="User/${message.get(MessageColumnNames.USERNAME) }">
					<c:out value="${message.get(MessageColumnNames.FIRST_NAME) }"/></a></p>
					
				<p class="date"><c:out value="${message.get(MessageColumnNames.DATE_POSTED) }"/></p>
				<p class="date"><c:out value="${message.get(MessageColumnNames.DATE_EDITED) }"/></p>
				<p class="msgBody"><c:out value="${message.get(MessageColumnNames.MESSAGE_BODY) }" escapeXml="false"/></p>
				<c:if test="${user.isAdmin() || user.get(UserColumnNames.USERNAME) == message.get(MessageColumnNames.USERNAME)}">
					
				</c:if>
			</div>
		</c:forEach>	
	</c:if>
	
</body>
</html>