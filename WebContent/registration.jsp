<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="constants.UserColumnNames" %>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src= "script/jquery-2.1.3.min.js"></script>
<script src= "script/main.js"></script>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/registration.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<script>
$(document).ready(function(){
	$('input, select, textarea').on("focus", function() {
        $(this).closest('.tile').addClass('shadowed');
    });
    $("input, select, textarea").on("blur",function() {
        $(this).closest('.tile').removeClass('shadowed');
    });

});
</script>
<title>Registration</title>
</head>
<body>
	<div class="regWrapper">
		<form action="Registration" method="post">
		<div class="tile darkBlue xwhole">
			<div class="tileContent">
				<p class="nameLabel">Personal Details</p><br>
				<div class="left">
					<p class="label">First Name</p>
					<input type="text" class="regText" name="fName" placeholder="First Name" id="fName"> 
				</div>
				<div class="left">
					<p class="label">Last Name</p>
					<input type="text" class="regText" name="lName" placeholder="Last Name" id="lName"> 
				</div>
				<div class="left">
				<p class="label">Birthday</p>
					<select name="year" class="regText year" id="year" onchange="dateChanged(this)">
						<!-- <option disabled>Year</option> -->
					</select>
					<select name="month" class="regText month" id="month" onchange="dateChanged(this)">
						<!-- <option disabled>Month</option> -->
					</select>
					<select name="day" class="regText day" id="day">
						<!-- <option disabled>Day</option> -->
					</select>
				</div>
			</div>
		</div>

		<div class="tile orange xhalf left">
			<div class="tileContent">
				<div class="userWrapper">
					<p class="nameLabel">Gender</p>
					<p class="label">Sex</p>
					<input type="radio" name="gender" onclick="setGender(this)" value="Male" checked>Male
					<input type="radio" name="gender" onclick="setGender(this)" value="Female">Female
					<br>
					<p class="label">Salutation</p>
					<select name="salutation" class="regText" id="salutation">
					</select>
				</div>
			</div>
		</div>
		<div class="tile gray xhalf right">
			<div class="tileContent">
				<div class="userWrapper">
					<p class="nameLabel">Account Details</p>
					<p class="label">Username</p>
					<input type="text" class="regText" name="username" placeholder="User Name" id="username" onblur="checkUsername()"><p id="userMsg"></p>
					<p class="label">Password</p>
					<input type="password" class="regText" name="password" placeholder="Password" id="pass"><br>
					<p class="label">Confirm Password</p>
					<input type="password" class="regText" name="confirmPassword" id="cpass" placeholder="Confirm Password" onblur="confirmPass()"><p id="errMsg" class="error"></p>
				</div>
			</div>
		</div>
		<br>
		<c:if test="${user!=null }">
			<c:if test="${user.get(UserColumnNames.IS_ADMIN)==1}">
				<div class="tile darkBlue xhalf left accType">
					<div class="tileContent">
						<div class="userWrapper">
							<p class="nameLabel">Account Type</p><br>
							<input type="radio" class="regSelect" name="isAdmin" value="user" checked>User
							<input type="radio" class="regSelect" name="isAdmin" value="admin">Admin
						</div>
					</div>
				</div>
			</c:if>
		</c:if>
		<div class="tile orange xwhole">
			<div class="tileContent">
				<div class="userWrapper">
					<p class="nameLabel">About Me</p>
					<textarea name="aboutMe" class="regArea"></textarea>
				</div>
			</div>
		</div>
		<div class="regSubmit">
			<input type="submit" value="Register">
		</div>
		</form>
	</div>
</body>
</html>