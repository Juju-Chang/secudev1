SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `darwin` DEFAULT CHARACTER SET utf8 ;
USE `darwin` ;

-- -----------------------------------------------------
-- Table `darwin`.`messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `darwin`.`messages` ;

CREATE TABLE IF NOT EXISTS `darwin`.`messages` (
  `message_id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `body` VARCHAR(255) NOT NULL,
  `dateEdited` DATETIME NULL DEFAULT NULL,
  `datePosted` DATETIME NOT NULL,
  PRIMARY KEY (`message_id`),
  INDEX `author_id` (`username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `darwin`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `darwin`.`users` ;

CREATE TABLE IF NOT EXISTS `darwin`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `fName` VARCHAR(50) NOT NULL,
  `lName` VARCHAR(50) NOT NULL,
  `isFemale` TINYINT(1) NOT NULL,
  `salutation` VARCHAR(10) NOT NULL,
  `birthDate` DATE NOT NULL,
  `username` VARCHAR(50) NOT NULL,
  `PW` VARBINARY(1000) NOT NULL,
  `aboutMe` VARCHAR(1000) NULL DEFAULT NULL,
  `isAdmin` TINYINT(1) NOT NULL,
  `encrypt` VARCHAR(64) NOT NULL,
  `dateJoined` DATE NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
/*
-- Query: select * from messages
LIMIT 0, 1000

-- Date: 2015-09-20 23:36
*/
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (9,'Username','By Username 3','2015-09-16 05:34:05','2015-09-16 05:09:25');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (11,'Username','1',NULL,'2015-09-16 05:43:20');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (12,'Username','2',NULL,'2015-09-16 05:43:23');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (13,'Username','3',NULL,'2015-09-16 05:43:25');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (14,'Username','4',NULL,'2015-09-16 05:43:26');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (15,'Username','5',NULL,'2015-09-16 05:43:27');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (16,'Username','6',NULL,'2015-09-16 05:43:29');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (17,'Username','7',NULL,'2015-09-16 05:43:31');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (18,'Username','8',NULL,'2015-09-16 05:43:32');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (19,'Username','9',NULL,'2015-09-16 05:43:33');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (20,'Username','10',NULL,'2015-09-16 05:43:36');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (21,'Username','11',NULL,'2015-09-16 05:56:32');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (23,'Username','13',NULL,'2015-09-16 05:56:36');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (24,'Username','14',NULL,'2015-09-16 05:56:38');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (25,'Username','15',NULL,'2015-09-16 05:56:40');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (26,'Username','16',NULL,'2015-09-16 05:56:41');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (27,'Username','17',NULL,'2015-09-16 05:56:43');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (28,'Username','18',NULL,'2015-09-16 05:56:45');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (29,'Username','19',NULL,'2015-09-16 05:56:47');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (30,'Username','20',NULL,'2015-09-16 05:56:49');
INSERT INTO `messages` (`message_id`,`username`,`body`,`dateEdited`,`datePosted`) VALUES (32,'Username','Edited again!','2015-09-20 23:13:44','2015-09-16 06:20:53');


INSERT INTO `users` (`user_id`,`fName`,`lName`,`isFemale`,`salutation`,`birthDate`,`username`,`PW`,`aboutMe`,`isAdmin`,`encrypt`,`dateJoined`) VALUES (1,'Renzo','Foiz',0,'Mr','1996-06-24','Doctor99',AES_ENCRYPT("whooster",'zW2AmRQhcabncvQljFspU/yjHS/sr6stibyfpnOf8Vo='),'',1,'zW2AmRQhcabncvQljFspU/yjHS/sr6stibyfpnOf8Vo=','2015-08-02');
INSERT INTO `users` (`user_id`,`fName`,`lName`,`isFemale`,`salutation`,`birthDate`,`username`,`PW`,`aboutMe`,`isAdmin`,`encrypt`,`dateJoined`) VALUES (2,'Juju','Chang',1,'Mrs','1995-08-24','QueenAnna',AES_ENCRYPT("burritos",'7iF4D0esAVmVdlILTkBQk55J/faImbg577WJ7ew/Jfw='),'',1,'7iF4D0esAVmVdlILTkBQk55J/faImbg577WJ7ew/Jfw=','2015-08-02');
INSERT INTO `users` (`user_id`,`fName`,`lName`,`isFemale`,`salutation`,`birthDate`,`username`,`PW`,`aboutMe`,`isAdmin`,`encrypt`,`dateJoined`) VALUES (3,'Metal','Kang',1,'Mr','1994-12-24','KingOfHeavyMet-ALL',AES_ENCRYPT("redhotchili",'EXAQm/86gVGqQwXzeLH+IZ5RXaN2IElqNvDro+jjQJM='),'',1,'EXAQm/86gVGqQwXzeLH+IZ5RXaN2IElqNvDro+jjQJM=','2015-08-02');
INSERT INTO `users` (`user_id`,`fName`,`lName`,`isFemale`,`salutation`,`birthDate`,`username`,`PW`,`aboutMe`,`isAdmin`,`encrypt`,`dateJoined`) VALUES (4,'Fourth','Username',0,'Mr','1909-01-01','Username',AES_ENCRYPT("TheFour",'gIynGBXKWsW3P4VTFzsEvcf5P003Rl3zvSJIIBxhSus='),'Username2',0,'gIynGBXKWsW3P4VTFzsEvcf5P003Rl3zvSJIIBxhSus=','2015-09-14');
INSERT INTO `users` (`user_id`,`fName`,`lName`,`isFemale`,`salutation`,`birthDate`,`username`,`PW`,`aboutMe`,`isAdmin`,`encrypt`,`dateJoined`) VALUES (6,'FirstPassword','Password',0,'Mr','1909-01-01','Password',AES_ENCRYPT("FirstPW",'hYMl7bgS2fGV04qHhkuOiJnJPXeeCOTqZumNmcom8Go='),'Password',0,'hYMl7bgS2fGV04qHhkuOiJnJPXeeCOTqZumNmcom8Go=','2015-09-18');