package controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.BackUp;
import services.Constants;
import services.DBService;

@WebServlet("/Backup")
public class Backup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Backup() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.getSession().setAttribute("allBackups", getAllBackups());		
    	response.sendRedirect("backup.jsp");
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fileName     	= request.getParameter("fileName");
		
		if(fileName != null){
			DBService dbService = new DBService();
			BackUp backup       = new BackUp(dbService.getAllmessages());
			backup.writeBackUp(fileName);
		}
		
		request.getSession().setAttribute("allBackups", getAllBackups());		
		response.sendRedirect("backup.jsp");
	}
	private ArrayList<String> getAllBackups(){
		ArrayList<String> backups = new ArrayList<String>();
		
		if(!Files.isDirectory(Paths.get(Constants.BU_DIR))){
			File newFolder = new File(Constants.BU_DIR);
			try{
				newFolder.mkdir();
				System.out.println("I have made a folder in :"+Constants.BU_DIR);
			}
			catch(SecurityException e){
				System.out.println(e.getMessage());
				System.out.println("I can't make a folder in :"+Constants.BU_DIR);
			}
		}
		
		File[] files = new File(Constants.BU_DIR+"/").listFiles();
		System.out.println(files.length);
		System.out.println("YOU GOT MORE COW BELL!");
		
		for (File file : files) {
		    if (file.isFile() && !file.getName().contains("DS_Store")) {
		        backups.add(file.getName());
		    }
		}
		return backups;
	}
}
