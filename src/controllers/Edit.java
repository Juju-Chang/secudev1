package controllers;

import java.io.IOException;
import java.util.EnumMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import services.DBService;
import constants.FieldNames;
import constants.UserColumnNames;
import models.User;
import services.Utilities;

@WebServlet("/Edit")
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public Edit() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String com = "Edit";
		EnumMap<FieldNames, String> values = Utilities.getValues(request, com);

		if(Utilities.validate(values,com)){
			DBService dbService = new DBService();
			dbService.updateUser(values);
			
			User user                   = dbService.login((String) values.get(FieldNames.USERNAME), (String) values.get(FieldNames.PASSWORD));
			HttpSession session         = request.getSession();
			session.setAttribute("user" , user);
			String username = user.get(UserColumnNames.USERNAME);
			session.setAttribute("userMessages", dbService.getMessagesByUser(username));
			
			if(user!=null)
				if(!user.isAdmin())
					request.setAttribute("user", null);

			RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		}
		else{
			RequestDispatcher rd = request.getRequestDispatcher("edit.jsp");
			rd.forward(request, response);
		}
	}
}
