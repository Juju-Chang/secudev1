package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constants.FieldNames;
import models.Message;
import models.User;
import services.DBService;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Login() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBService db = new DBService();
		ArrayList<Message> m = new ArrayList<Message>();
		
		m = db.get10Messages(0);
		
		String username = request.getParameter(FieldNames.USERNAME.toString());
		String password = request.getParameter(FieldNames.PASSWORD.toString());
		
		User user = db.login(username, password);
		if(user!=null && username!=null && password!=null){
			System.out.println("You in bruh!");
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			session.setAttribute("allMessages", m);
			session.setAttribute("userMessages", db.getMessagesByUser(username));
			request.removeAttribute("flashMessage");
		}
		else{
			request.setAttribute("flashMessage", "Wrong username or password");
			System.out.println("Sorry nah");
		}
		response.sendRedirect("index.jsp");
	}
}