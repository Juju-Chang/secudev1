package controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import services.DBService;
import services.Utilities;
import models.Message;
import models.User;
import constants.MessageColumnNames;
import constants.UserColumnNames;

@WebServlet("/Message")
public class MessageController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public MessageController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Message> m = new ArrayList<Message>();
		DBService dbService  = new DBService();
		m                    = dbService.get10Messages(0);
		
		HttpSession session = request.getSession();
		session.setAttribute("allMessages", m);
		
		response.sendRedirect("AllMessages/0");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date date            = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String datePosted    = sdf.format(date);
		
		EnumMap<MessageColumnNames, String> values = new EnumMap<>(MessageColumnNames.class);
		User user = (User) request.getSession().getAttribute("user");
		
		if(user!=null){
			values.put(MessageColumnNames.MESSAGE_BODY	, Utilities.cleanShit(request.getParameter(MessageColumnNames.MESSAGE_BODY.toString())));
			values.put(MessageColumnNames.DATE_POSTED	, datePosted);
			values.put(MessageColumnNames.USERNAME		, user.get(UserColumnNames.USERNAME));
			
			DBService dbService = new DBService();
			System.out.println(values.get(MessageColumnNames.MESSAGE_BODY).toString());
			if(!values.get(MessageColumnNames.MESSAGE_BODY).toString().contains("Secudev1/Logout")){
				dbService.newMessage(values);
			}
			ArrayList<Message> m = dbService.getMessagesByUser(user.get(UserColumnNames.USERNAME));
			
			HttpSession session = request.getSession();
			session.setAttribute("userMessages", m);
		}
		response.sendRedirect("Message");
	}

}
