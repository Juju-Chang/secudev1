package controllers;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.Constants;

@WebServlet("/Download")
public class Download extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Download() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String filename = request.getParameter("filename");
		System.out.println("File name is:"+filename);
		
		if(filename != null){
			if(filename.endsWith(".csv")){
				File f = new File(Constants.BU_DIR+filename);
				
				if(f.exists()){
					response.setContentType("text/csv");
				    response.setHeader("Content-Disposition", "attachment; filename="+ filename);
				    try {
						OutputStream outputStream = response.getOutputStream();
						byte[] byteBuffer         = new byte[1024];
						DataInputStream dis       = new DataInputStream(new FileInputStream(Constants.BU_DIR + filename));
						int length                = 0;
				       
				       while((dis != null) && ((length = dis.read(byteBuffer)) != -1)){
				    	   outputStream.write(byteBuffer,0,length);
				       }
				       
				        outputStream.flush();
				        outputStream.close();
				        dis.close();
				    }
				    catch(Exception e) {
				        System.out.println(e.toString());
				    }
				}
				else{
					System.out.println("File "+filename+" does not exist");
					response.sendRedirect("backup.jsp");
				}
			}
			else{
				System.out.println("Invalid extention");
				response.sendRedirect("backup.jsp");
			}
		}
	}
}
