package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constants.MessageColumnNames;
import constants.UserColumnNames;
import models.Message;
import models.User;
import services.DBService;

@WebServlet("/EditMessage")
public class EditMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public EditMessage() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String edited   = request.getParameter("msgEdit") == null? "": request.getParameter("msgEdit");
		String id       = request.getParameter("msgId")   == null? "": request.getParameter("msgId"); 
		DBService db    = new DBService();
		User user       = (User) request.getSession().getAttribute("user");
		Message message = db.getMessage(id);

		if(user != null && message != null){
			if(user.isAdmin()||user.get(UserColumnNames.USERNAME).equals(message.get(MessageColumnNames.USERNAME))){
				db.updateMessageContent(message.get(MessageColumnNames.MESSAGE_ID), edited);
				String username = user.get(UserColumnNames.USERNAME);
				HttpSession session = request.getSession();
				session.setAttribute("userMessages", db.getMessagesByUser(username));
			}
		}
		response.sendRedirect("Message");
	}
}
