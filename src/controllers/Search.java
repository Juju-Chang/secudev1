package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.DateFilter;
import models.Filter;
import models.UsernameFilter;
import services.DBService;
import constants.SearchNames;

@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Search() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String search       = request.getParameter(SearchNames.QUERY.toString());
		HttpSession session = request.getSession();
		
    	if(session.getAttribute("filterList") == null)
    		session.setAttribute("filterList", new ArrayList<Filter>());
    	
    	@SuppressWarnings("unchecked")
		ArrayList<Filter> filterList = (ArrayList<Filter>) session.getAttribute("filterList");

    	String ao       	= request.getParameter("ao");
    	if(ao != null){
			String username = request.getParameter("username");
			String dateCond = request.getParameter("dateCond");
			
    		if(username != null){
    			filterList.add(new UsernameFilter(ao, username));
    		}
    		else if(dateCond != null){
    			String year       = request.getParameter("dateYear");
				String month      = request.getParameter("dateMonth");
				String day        = request.getParameter("dateDay");
				String comparator = request.getParameter("dateCond");
				
				if(year!=null && month!=null && day!=null){
					filterList.add(new DateFilter(year, month, day, comparator, ao));
				}
    		}
    	}
    	if(search!=null){
    		DBService dbService = new DBService();
    		session.setAttribute("searchResults", dbService.getMessagesFromSearch2(search, filterList));
    		response.sendRedirect("search.jsp");
    	}
    	response.getWriter().write("true");
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("del")!= null){
			HttpSession session = request.getSession();
			
    		@SuppressWarnings("unchecked")
			ArrayList<Filter> filterList = (ArrayList<Filter>) session.getAttribute("filterList");
    		
    		filterList.remove(Integer.parseInt(request.getParameter("del")));
		}
	}
}
