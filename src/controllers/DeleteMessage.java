package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Message;
import models.User;
import constants.MessageColumnNames;
import constants.UserColumnNames;
import services.DBService;

@WebServlet("/DeleteMessage/*")
public class DeleteMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteMessage() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Nakarating sa do get");
		String id = request.getPathInfo() == null? "" : request.getPathInfo();
		
		System.out.println("Id is: "+id.substring(1));
		if(id.length()>1){
			id              = id.substring(1);
			DBService db    = new DBService();
			
			User user       = (User) request.getSession().getAttribute("user");
			Message message = db.getMessage(id);
			
			if(user != null && message != null){
				if(user.isAdmin()||user.get(UserColumnNames.USERNAME).equals(message.get(MessageColumnNames.USERNAME))){
					db.deleteMessage(id);
					String username = user.get(UserColumnNames.USERNAME);
					HttpSession session = request.getSession();
					session.setAttribute("userMessages", db.getMessagesByUser(username));
				}
			}
		}
		response.sendRedirect("../Message");
	}
}
