package controllers;

import java.io.IOException;
import java.util.EnumMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import services.DBService;
import services.Utilities;
import constants.FieldNames;
import constants.Gender;
import constants.Salutation;
import constants.TableNames;

@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Registration() {
        super();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Gender gender   = Gender.fromObject(request.getParameter(FieldNames.GENDER.toString()));
    	String username = request.getParameter(FieldNames.USERNAME.toString());
    	
    	if(gender!= Gender.INVALID){
    		System.out.println(gender);
    		String options = "";

    		Salutation[] names = Salutation.values();
    		for(int i = 0; i < names.length; i++){
				if(names[i].getGender() == gender)
    				options += "<option value=\'"+ names[i].name() +"\'>"+names[i].name()+"</option>";
    		}
    		response.getWriter().write(options);
    	}
    	else if(username!= null){
    		DBService db = new DBService();
    		response.getWriter().write(Boolean.toString(db.rowExists(username, TableNames.USERS)));
    	}
	}
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String com = "Registration";
		EnumMap<FieldNames, String> values = Utilities.getValues(request, com);
		
		if(Utilities.validate(values,com)){
			DBService dbService = new DBService();
			dbService.register(values);
			
			User user           = (User) request.getSession().getAttribute("user");
			
			if(user!=null)
				if(!user.isAdmin())
					request.setAttribute("user", null);

			RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		}
		else{
			RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
			rd.forward(request, response);
		}
	}
}



