package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns =   {"/edit.jsp", "/Edit", 
							"/messages.jsp/*", "/AllMessages/*",
							"/EditMessage/*", "/DeleteMessage/*",
							"/Search", "/search.jsp"})
public class NoUserFilter implements Filter {	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("Init No User filter");
	}

	@Override
	public void destroy() {
		System.out.println("Destroy No User filter");
	}

	@Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest  request  = (HttpServletRequest)  req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session          = request.getSession();
		
        if (session == null || session.getAttribute("user") == null) {
        	response.sendRedirect("http://localhost:8080/Secudev1/login.jsp");
        }
		else {
			chain.doFilter(req, res); // Logged-in user found, so just continue request.
        }
	}
}


