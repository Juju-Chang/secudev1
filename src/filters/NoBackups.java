package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns =   {"/backup.jsp"})
public class NoBackups implements Filter {

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("Init No Backup filter");
	}

	@Override
	public void destroy() {
		System.out.println("Destroy No Backup filter");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest  request  = (HttpServletRequest)  req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session          = request.getSession();
		
		if (session == null || session.getAttribute("allBackups") == null) {
			response.sendRedirect("http://localhost:8080/Secudev1/Backup");
		}
		else{
			chain.doFilter(req, res);
		}
	}
}
