package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.User;

@WebFilter(urlPatterns =   {"/backup.jsp", "/Backup", "/Download"})
public class NotAdmin implements Filter {
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("Init Not Admin Filter");
	}

	@Override
	public void destroy() {
		System.out.println("DestroyNot Admin Filter");
	}
	
	@Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest  request  = (HttpServletRequest)  req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session          = request.getSession();		
		
		
		if (session != null) {
			User user = (User)session.getAttribute("user");
			if(user!=null){
				if(user.isAdmin()){
					chain.doFilter(req, res); 
				} else{
					response.sendRedirect("http://localhost:8080/Secudev1/index.jsp");
				}
			} else{
				response.sendRedirect("http://localhost:8080/Secudev1/login.jsp");
			}
		}else{
			response.sendRedirect("http://localhost:8080/Secudev1/login.jsp");
		}
	}
}
