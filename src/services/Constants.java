package services;

public abstract class Constants {
	public static final String DB_URL      = "jdbc:mysql://localhost:3306/darwin";
	public static final String DB_USER     = "root";
	public static final String DB_PASSWORD = "password";
	public static final String BU_DIR      = System.getProperty("user.home") + "/Desktop/Backup/";
//	public static final String WIN_BU_DIR  = System.getProperty("user.home") + "\\Desktop\\Backup";
//	public static final String MAC_BU_DIR  = System.getProperty("user.home") + "/Desktop/Backup";
}