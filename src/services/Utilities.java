package services;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import models.User;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import constants.FieldNames;
import constants.Gender;
import constants.Salutation;
import constants.TableNames;
import constants.UserColumnNames;

public class Utilities {
	private static String setIsAdmin(HttpServletRequest request){
		HttpSession session = request.getSession();
		User user           = (User) session.getAttribute("user");
		
		if(user!=null && request.getParameter(FieldNames.IS_ADMIN.toString())!=null){
			System.out.println("Boolean setIsAdmin There's a user");
			System.out.println(user.isAdmin());
			
			if(user.isAdmin()){
				return request.getParameter(FieldNames.IS_ADMIN.toString()).equalsIgnoreCase("admin")? "1" : "0";
			}
		}
		else{
			System.out.println("Boolean setIsAdmin No user");
		}
		return "0";
	}
	
	private static boolean validateUniqueUsername(String username) {
		DBService dbService = new DBService();
		return dbService.rowExists(username, TableNames.USERS);
	}

		
	private static boolean validateNotEmpty(String[] toValidate){
		for(String s:toValidate){
			if(s.isEmpty()){
				return false;
			}
		}
		return true;
	}
	
	private static boolean validateLength(String[] toValidate, int limit){
		for(String s: toValidate){
			if(s.length()>limit && !s.isEmpty()){
				return false;
			}
		}
		return true;
	}
	
	private static boolean validateNotPresent(String toValidate, String regex){
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(toValidate);
		
		return !matcher.find();
	}
	
	private static boolean validateSalutation(Gender gender, Salutation salutation){
		if(salutation == Salutation.INVALID || gender == Gender.INVALID)
			return false;
		else
			return salutation.getGender() == gender;
	}													//Ex. Female is a mrs									
	
	private static boolean validateDate(String sYear, String sMonth, String sDay){
		try{
			int month      = Integer.parseInt(sMonth);
			int day        = Integer.parseInt(sDay);
			
			int upperLimit = 0;
			
			switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: upperLimit = 31; break;
			
			case 4:
			case 6:
			case 9:
			case 11: upperLimit = 30; break;
			
			case 2:  upperLimit = 28; break;
			default: return false;
			}
			
			if(day<1 || day>upperLimit){
				return false;
			}
			return true;
		}
		catch(Exception e){
			System.err.println(e.getMessage());
		}
		return false;
	}
	private static boolean validateSamePassword(String password, String confirmPassword){
		return password.equals(confirmPassword);
	}
	private static boolean validateBirthday(String sYear, String sMonth, String sDay){
		try{
			int year       = Integer.parseInt(sYear);
			int month      = Integer.parseInt(sMonth);
			int day        = Integer.parseInt(sDay);
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -18);
			Date minus18 = cal.getTime();
			cal.set(year, month, day);
			
			if(cal.getTime().before(minus18) || cal.getTime().equals(minus18))
				return true;
		}catch(DateTimeParseException e){
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public static boolean validateNotNull(Object[] values){
		for(Object o: values){
			if(o==null){
				System.out.println(o+" is null");
				return false;
			}
		}
		return true;
	}
	
	public static EnumMap<FieldNames, String> getValues(HttpServletRequest request, String com){
		EnumMap<FieldNames, String> values = new EnumMap<>(FieldNames.class);
		
		for(FieldNames f: FieldNames.getAllExceptUsername()){
			if(f != FieldNames.IS_ADMIN){
				values.put(f, request.getParameter(f.toString()));
			}
			else{
				values.put(f, setIsAdmin(request));
			}
		}
		
		if(com.equalsIgnoreCase("Edit")){
			User dude = (User) request.getSession().getAttribute("user");
			values.put(FieldNames.USERNAME   , dude.get(UserColumnNames.USERNAME));
		}
		else{
			values.put(FieldNames.USERNAME   , request.getParameter(FieldNames.USERNAME.toString()));
		}
		return values;
	}
	
	public static boolean validate(EnumMap<FieldNames, String> values, String com){
		boolean allValid = true;
		
		String fName               = values.get(FieldNames.F_NAME);
		String lName               = values.get(FieldNames.L_NAME);
		
		Gender isFemale            = Gender.fromObject(values.get(FieldNames.GENDER));
		Salutation salutation      = Salutation.fromObject(values.get(FieldNames.SALUTATION));
		
		String year                = values.get(FieldNames.YEAR);
		String month               = values.get(FieldNames.MONTH);
		String day                 = values.get(FieldNames.DAY);
		
		String username            = values.get(FieldNames.USERNAME);
		String password            = values.get(FieldNames.PASSWORD);
		String confirmPassword     = values.get(FieldNames.CONFIRM_PASSWORD);
		
		if(!validateNotNull(new Object[]{fName, lName, isFemale, salutation, year, month, day, username, password, confirmPassword})){
			return false;
		}

		ArrayList<Boolean> toValidate = new ArrayList<>();
			toValidate.add(validateNotPresent(fName+lName, "[^a-zA-Z0-9\\s]"));
			toValidate.add(validateNotPresent(username+password, "[^\\w]"));
			
			toValidate.add(validateSalutation(isFemale, salutation));
	
			toValidate.add(validateBirthday(year, month, day));
			toValidate.add(validateDate	(year, month, day));
			
			toValidate.add(validateSamePassword(password, confirmPassword));
			
			toValidate.add(validateLength(new String[]{fName, lName, username}, 50));			
			
			if(com.equalsIgnoreCase("Registration")){
				toValidate.add(validateUniqueUsername(username));
				toValidate.add(validateNotEmpty(new String[]{fName, lName, salutation.toString(), year, month, day, username, password}));
			}
			
		for(int i=0;i<toValidate.size();i++){
			if(!toValidate.get(i)){
				allValid = false;
				System.out.println("Error at toValidate index: "+i);
			}
		}
		return allValid;
	}
	
	public static String cleanShit(String badShit){
		String goodShit = "";
		goodShit = Jsoup.clean(badShit, Whitelist.basicWithImages());
		return goodShit;
	}
}