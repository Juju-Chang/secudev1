package services;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.Random;
import java.text.SimpleDateFormat;

import org.apache.tomcat.util.codec.binary.Base64;

import models.Filter;
import models.Item;
import models.Message;
import models.User;
import constants.FieldNames;
import constants.Gender;
import constants.MessageColumnNames;
import constants.TableNames;
import constants.UserColumnNames;

public class DBService extends Constants{
	public void register(EnumMap<FieldNames, String> values){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

			final Random r       = new SecureRandom();
			byte[] saltByte      = new byte[32];
			r.nextBytes(saltByte);
			String salt          = Base64.encodeBase64String(saltByte);
			
			Date date            = Calendar.getInstance().getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String dateJoined    = sdf.format(date);
			
			String insertTableSQL = "INSERT INTO darwin.users"
					+ "(fName, lName, isFemale, salutation, birthDate, "
					+ "username, PW, "
					+ "aboutMe, isAdmin, encrypt, dateJoined) VALUES"
					+ "(?,?,?,?,?,"
					+ "?,AES_ENCRYPT(?,?),"
					+ "?,?,?,?)";
			
			PreparedStatement p = conn.prepareStatement(insertTableSQL);
			
			p.setString(1, values.get(FieldNames.F_NAME));
			p.setString(2, values.get(FieldNames.L_NAME));
			p.setInt   (3, Gender.fromObject(values.get(FieldNames.GENDER)).getOpcode());
			p.setString(4, values.get(FieldNames.SALUTATION));
			
			p.setString(5, values.get(FieldNames.YEAR)+"-"+
						   values.get(FieldNames.MONTH)+"-"+
						   values.get(FieldNames.DAY));
			
			p.setString(6, values.get(FieldNames.USERNAME));
			p.setString(7, values.get(FieldNames.PASSWORD));
			p.setString(8, salt);
			
			p.setString(9, values.get(FieldNames.ABOUT_ME));
			p.setString(10,values.get(FieldNames.IS_ADMIN));
			p.setString(11,salt);
			p.setString(12,dateJoined);
			
			p.executeUpdate();
		}catch(Exception e){
			catchException(e);
		}
	}

	public User login(String username, String password){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn        = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			PreparedStatement salt = conn.prepareStatement("SELECT encrypt FROM darwin.users where username =?");
			salt.setString(1, username);
			
			String saltString      = "";
			ResultSet resultSalt   = salt.executeQuery();
			
			if(resultSalt.next()){
				saltString = resultSalt.getString(UserColumnNames.ENCRYPT.toString());
			}
			
			PreparedStatement p = conn.prepareStatement(  "SELECT * FROM darwin.users WHERE username = ? AND "
														+ "PW = AES_ENCRYPT(?,?)");
			p.setString(1, username);
			p.setString(2, password);
			p.setString(3, saltString);
			
			ResultSet rs = p.executeQuery();
			
			if(rs.next()){
				EnumMap<UserColumnNames, Object> hm = new EnumMap<>(UserColumnNames.class);
				for(UserColumnNames cn: UserColumnNames.values()){
					hm.put(cn, rs.getString(cn.toString()));
				}
				return new User(hm);
			}
		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	
	public ArrayList<Message> getMessagesWhereDate(String comparator, String date){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn     = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM darwin.messages WHERE datePosted ? ?");
			pstmt.setString(1, comparator);
			pstmt.setString(2, date);
			
			System.out.println("Date sql is: "+pstmt);

			return getMessageFromResultSet(pstmt.executeQuery(), null);
		}catch(Exception e){
			catchException(e);
		}
		return new ArrayList<Message>();
	}

	public Message getMessage(String id){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn     = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM darwin.messages WHERE message_id =?");
			pstmt.setString(1, id);
			
			return getMessageFromResultSet(pstmt.executeQuery(), null).get(0);
		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	
	public Item getItem(String itemName){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn     = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			PreparedStatement p = conn.prepareStatement("SELECT * FROM Darwin.ITEMS where name =?");
			p.setString(1, itemName);
			
			ResultSet rs = p.executeQuery();
			
			if(rs.next()){
				return new Item(
					rs.getString("name"),
					rs.getString("description"),
					rs.getString("imageSource"),
					rs.getInt("price")
				);
			}
			
		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	
	public String getMessageContents(String id){
		Message m = getMessage(id);
		
		if(m!=null){
			return m.get(MessageColumnNames.MESSAGE_BODY);
		}
		return null;
	}
	
	public User getUserfromUsername(String username){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn     = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			ArrayList<UserColumnNames> neededColumns = new ArrayList<>();
				neededColumns.add(UserColumnNames.USERNAME);
				neededColumns.add(UserColumnNames.SALUTATION);
				neededColumns.add(UserColumnNames.FIRST_NAME);
				neededColumns.add(UserColumnNames.LAST_NAME);
				neededColumns.add(UserColumnNames.BIRTHDATE);
				neededColumns.add(UserColumnNames.ABOUT_ME);
				neededColumns.add(UserColumnNames.IS_FEMALE);
			
			String query = "SELECT ";
			
			for(int i=0;i<neededColumns.size();i++){
				query+=neededColumns.get(i).toString();
				if(i != neededColumns.size()-1)
						query+=", ";
			}
			
			query+=" FROM users WHERE username = ?";
			
			
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, username);
			
			ResultSet rs = p.executeQuery();
			
			if(rs.next()){
				EnumMap<UserColumnNames, Object> hm = new EnumMap<>(UserColumnNames.class);
				
				for(UserColumnNames cn: neededColumns){
					if(rs.getString(cn.toString())!=null){
						hm.put(cn, rs.getString(cn.toString()));
					}
				}
				User user = new User(hm);
				return user;
			}

		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	
	public boolean rowExists(String searchName, TableNames tableName) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn     = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			PreparedStatement p = conn.prepareStatement("SELECT * FROM darwin."+tableName+" WHERE username = ?");
			p.setString(1, searchName);
			
			System.out.println("Tablenames in pstsmt is "+p);
			
			ResultSet rs = p.executeQuery();
			
			return !rs.next();				//rs.next=true ->  returned a user. That's why we ! it.
		}catch(Exception e){				//rs.next=false -> no user (Unique name)
			catchException(e);
		}
		return false;
	}
	
	public void updateUser(EnumMap<FieldNames, String> values){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			final Random r = new SecureRandom();
			byte[] saltByte = new byte[32];
			r.nextBytes(saltByte);
			String salt = Base64.encodeBase64String(saltByte);
			
			String sqlCode = "UPDATE darwin.users SET "
					+ "fName = IF(? <> '', ?, fName), "
					+ "lName = IF(? <> '', ?, lName), "
					+ "isFemale = ?, salutation = ?, birthDate = ?, ";
			
				if(!values.get(FieldNames.PASSWORD).isEmpty()){
					sqlCode+="PW = AES_ENCRYPT(?,?), encrypt = ?, ";
						
					}
			sqlCode +="aboutMe = IF(? <> '', ?, aboutMe), "
					+ "isAdmin = ? "
					+ "WHERE username = ?";
			
			PreparedStatement p = conn.prepareStatement(sqlCode);
			
			int i=1;
			p.setString(i, values.get(FieldNames.F_NAME));i++;
			p.setString(i, values.get(FieldNames.F_NAME));i++;
			p.setString(i, values.get(FieldNames.L_NAME));i++;
			p.setString(i, values.get(FieldNames.L_NAME));i++;
			p.setInt   (i, Gender.fromObject(values.get(FieldNames.GENDER)).getOpcode());i++;
			p.setString(i, values.get(FieldNames.SALUTATION));i++;
			
			p.setString(i, values.get(FieldNames.YEAR)+"-"+
						   values.get(FieldNames.MONTH)+"-"+
						   values.get(FieldNames.DAY));i++;
						   
		   if(!values.get(FieldNames.PASSWORD).isEmpty()){
	 			p.setString(i, values.get(FieldNames.PASSWORD));i++;
				p.setString(i, salt);i++;
				p.setString(i, salt);i++;
		   }
			
			p.setString(i, values.get(FieldNames.ABOUT_ME));i++;
			p.setString(i, values.get(FieldNames.ABOUT_ME));i++;
			p.setString(i, values.get(FieldNames.IS_ADMIN));i++;
			p.setString(i, values.get(FieldNames.USERNAME));i++;
			
			System.out.println(p);
			p.executeUpdate();
			
			System.out.println("Query Executed");
			
		}catch(Exception e){
			catchException(e);
		}
	}
	
	public void newMessage(EnumMap<MessageColumnNames, String> values){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			String sqlCode = "INSERT INTO darwin.messages "
					+ "(username, body, datePosted)"
					+ " VALUES (?,?,?)";
			
			PreparedStatement p = conn.prepareStatement(sqlCode);
			p.setString (1, values.get(MessageColumnNames.USERNAME));
			p.setString	(2, values.get(MessageColumnNames.MESSAGE_BODY));
			p.setString	(3, values.get(MessageColumnNames.DATE_POSTED));
			
			System.out.println(p);
			p.executeUpdate();
			
			System.out.println("Query Executed");
			
		}catch(Exception e){
			catchException(e);
		}
	}
	
	public int getMessageCount(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
    		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    		
    		Statement stmt = con.createStatement();
    		ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM darwin.messages");
    		return rs.getInt(1);
		}catch(Exception e){
			catchException(e);
		}
		return -1;
	}
	
	public ArrayList<Message> getAllmessages(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
    		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    		
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM darwin.messages");
            
            return getMessageFromResultSet(pstmt.executeQuery(), null);
		}catch(Exception e){
			catchException(e);
		}
    	return null;		
	}
	
	public ArrayList<Message> get10Messages(int pageNum){
		try{
			Class.forName("com.mysql.jdbc.Driver");
    		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    		
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM darwin.messages ORDER BY datePosted DESC LIMIT ?, 10");
            pstmt.setInt(1, pageNum*10);
            
            return getMessageFromResultSet(pstmt.executeQuery(), null);
		}catch(Exception e){
			catchException(e);
		}
    	return null;
	}
	
	public ArrayList<Message> getMessagesByUser(String username){
		try{
			Class.forName("com.mysql.jdbc.Driver");
    		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    		
            PreparedStatement prep = con.prepareStatement("SELECT * FROM darwin.messages WHERE username = ? ORDER BY datePosted DESC");
            prep.setString(1, username);
            
            return getMessageFromResultSet(prep.executeQuery(), getUserfromUsername(username));
		}catch(Exception e){
			catchException(e);
		}
    	return new ArrayList<Message>();
	}
	
	public boolean canEdit(String messageId, String username){
		try{
			Class.forName("com.mysql.jdbc.Driver");
    		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    		
    		User user = getUserfromUsername(username);
    		if(user!=null){
    			if(user.isAdmin()){
    				return true;
    			}
    			else{
	    			PreparedStatement stmt = con.prepareStatement("SELECT * FROM darwin.messages WHERE message_id=? AND username = ?");
	    			stmt.setString(1, messageId);
	    			stmt.setString(2, username);
	    			
	    			ResultSet rs = stmt.executeQuery();
	    			
	    			if(rs.next()){
	    				return true;
	    			}
    			}
    		}
		}catch(Exception e){
			catchException(e);
		}
		return false;
	}
	
	public void updateMessageContent(String id, String content){
		try{
			Class.forName("com.mysql.jdbc.Driver");
    		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    		
    		Date date            = Calendar.getInstance().getTime();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		String dateEdited    = sdf.format(date);
    		
    		Message message = getMessage(id);
    		
    		if(message!=null){
	    		PreparedStatement stmt = con.prepareStatement("UPDATE darwin.messages SET body = ?, dateEdited = ? "
	    				+ "WHERE message_Id = ? AND username = ?");
	    		stmt.setString(1, content);
	    		stmt.setString(2, dateEdited);
	    		stmt.setString(3, message.get(MessageColumnNames.MESSAGE_ID));
	    		stmt.setString(4, message.get(MessageColumnNames.USERNAME));
	    		
	    		stmt.executeUpdate();
    		}
		}catch(Exception e){
			catchException(e);
		}
	}
	
	public void deleteMessage(String id){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
    		PreparedStatement stmt = con.prepareStatement("DELETE FROM darwin.messages WHERE message_id = ?");
    		stmt.setString(1, id);

    		stmt.execute();
		}catch(Exception e){
			catchException(e);
		}
	}
	
	private String getEscapedPStatementSql(PreparedStatement pstmt){
		return pstmt.toString().substring( pstmt.toString().indexOf( ": " ) + 2 );
	}
	
	public String getEscapedUsername(String andOr, String username){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con          = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			String sql              = andOr+"username = ?";
			
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, username);
			
			return getEscapedPStatementSql(pstmt);
		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	
	public String getEscapedDate(String andOr, String comparator, String year, String month, String day){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con          = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			String sql              = andOr+"datePosted "+comparator+" ?";
			
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, year+"-"+month+"-"+day);
			
			return getEscapedPStatementSql(pstmt);
		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	public ArrayList<Message> getMessagesFromSearch2(String query, ArrayList<Filter> filters){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con          = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			String sql              = "SELECT * FROM darwin.messages WHERE (body LIKE ? or body = null) ";
			
			for(Filter f: filters){
				if(f!=null){
					sql+=f.toString();
				}
			}
			
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, "%"+query+"%");
			System.out.println("Search pstmt = "+pstmt);
			
			return getMessageFromResultSet(pstmt.executeQuery(), null);
		}catch(Exception e){
			catchException(e);
		}
		return null;
	}
	
	public int getNumOfPages(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
    		PreparedStatement stmt = con.prepareStatement("SELECT COUNT(*) FROM darwin.messages");

    		ResultSet rs = stmt.executeQuery();
    		if(rs.next()){
    			if(rs.getInt(1) % 10 == 0){
    				return rs.getInt(1)/10;
    			}
    			else{
    				return rs.getInt(1)/10 + 1;
    			}
    		}
		}catch(Exception e){
			catchException(e);
		}
		return -1;
	}

	private ArrayList<Message> getMessageFromResultSet(ResultSet rs, User user) throws SQLException{
		ArrayList<Message> messages= new ArrayList<>();
		
		while(rs.next()){
	    	EnumMap<MessageColumnNames, String> m = new EnumMap<>(MessageColumnNames.class);
	    	
			for(MessageColumnNames key: MessageColumnNames.valuesExceptFName())
				m.put(key, rs.getString(key.toString()));
			
			if(user!=null)
	        	m.put(MessageColumnNames.FIRST_NAME, user.get(UserColumnNames.FIRST_NAME));
			else{
				User messageUser = getUserfromUsername(m.get(MessageColumnNames.USERNAME));
				if(messageUser!=null){
					m.put(MessageColumnNames.FIRST_NAME, messageUser.get(UserColumnNames.FIRST_NAME));
				}
				else{
					m.put(MessageColumnNames.FIRST_NAME, "DELETED USER");
				}
			}
			messages.add(new Message(m));
		}
		return messages;
	}
	
	private void catchException(Exception e){
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
}