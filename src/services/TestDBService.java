package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class TestDBService extends Constants{
	public void deleteUser(String username){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			PreparedStatement pstmt = con.prepareStatement("DELETE FROM darwin.users WHERE username = ?");
			pstmt.setString(1, username);
			
			pstmt.executeUpdate();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
