package services;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import constants.MessageColumnNames;
import models.Message;

public class BackUp{
	private ArrayList<Message> msgs;
	
	public BackUp(ArrayList<Message> msgs){
		this.msgs = msgs;
	}
	
	public void writeBackUp(String filename){
		if(!filename.endsWith(".csv"))
			filename += ".csv";
			
		try {
			FileWriter writer;
			String OS = System.getProperty("os.name");
			Date date            = Calendar.getInstance().getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 'at' HH.mm.ss");
			String datePosted    = sdf.format(date);
			
			if(OS.contains("Mac")){
				System.out.println("Issa Mac");
				writer = new FileWriter(new File(Constants.BU_DIR+"/"+datePosted+" - "+filename));
			}
			else{
				System.out.println("Windows");
				writer = new FileWriter(new File(Constants.BU_DIR+datePosted+" - "+filename));
			}
			
			if(msgs.size() > 0){
				writer.append("Username");
				writer.append(',');
				writer.append("Date Posted");
				writer.append(',');
				writer.append("Post");
				writer.append("\n");
			}
			
			for(int i = 0 ; i < msgs.size(); i++){
				Message msg = msgs.get(i);
				
				writer.append(msg.get(MessageColumnNames.USERNAME));
				writer.append(',');
				writer.append(msg.get(MessageColumnNames.DATE_POSTED));
				writer.append(',');
				writer.append(msg.get(MessageColumnNames.MESSAGE_BODY));
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
