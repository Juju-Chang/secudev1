package models;

import services.DBService;

public class UsernameFilter extends Filter{
	private String username;

	public UsernameFilter(String isAnd, String username){
		super(isAnd);
		this.username = username;
	}


	@Override
	public String toString(){
		if(super.toString()!=null){
			DBService dbService = new DBService();
			return dbService.getEscapedUsername(super.toString(), username);
		}
		return null;
	}
}