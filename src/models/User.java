package models;

import java.util.EnumMap;
import java.util.Map.Entry;
import java.io.Serializable;

import constants.UserColumnNames;

public class User implements Serializable{
	private static final long	serialVersionUID	= 803090209617367315L;
	
	private EnumMap<UserColumnNames, Object> properties;
	
	public User(EnumMap<UserColumnNames, Object> userStats){
		this.properties = userStats;
	}
	public String get(UserColumnNames c){
		return (String)properties.get(c);
	}
	public boolean isAdmin(){
		String sIsAdmin = get(UserColumnNames.IS_ADMIN);
		if(sIsAdmin == null)
			return false;
		return sIsAdmin.equals("1");
	}
	@Override
	public String toString(){
		String result ="";
		for (Entry<UserColumnNames, Object> entry : properties.entrySet()) {
		    result+=entry.getKey()+" = "+entry.getValue()+"; ";
		}
		return result;
	}
}
