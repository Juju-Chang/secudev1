package models;

import java.util.EnumMap;
import java.util.Map.Entry;

import java.io.Serializable;

import constants.MessageColumnNames;

public class Message  implements Serializable{
	private static final long	serialVersionUID	= 803090209617367315L;
	
	private EnumMap<MessageColumnNames, String> properties;
	
	public Message(EnumMap<MessageColumnNames, String> userStats){
		this.properties = userStats;
	}
	public String get(MessageColumnNames c){
		return (String)properties.get(c);
	}
	@Override
	public String toString(){
		String result ="";
		for (Entry<MessageColumnNames, String> entry : properties.entrySet()) {
		    result+=entry.getKey()+" = "+entry.getValue()+"; ";
		}
		return result;
	}
}
