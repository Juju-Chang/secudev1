package models;

import services.DBService;
import constants.ValidOperators;

public class DateFilter extends Filter{
	private String year;
	private String month;
	private String day;
	private String comparator;

	public DateFilter(String year, String month, String day, String comparator, String isAnd){
		super(isAnd);
 		this.year       = year;
		this.month      = month;
		this.day        = day;
		this.comparator = comparator;
	}

	@Override
	public String toString(){
		if(!ValidOperators.withinDateOperators(comparator)){
			return "";
		}
		if(!super.toString().isEmpty()){
			DBService dbService = new DBService();
			return dbService.getEscapedDate(super.toString(), comparator, year, month, day);
		}
		return "";
	}
}
