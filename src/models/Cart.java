package models;

import java.util.ArrayList;

import services.DBService;

public class Cart {
	private static Cart cartSingleton = null;
	private ArrayList<Item> items;
	
	protected Cart(){
		items = new ArrayList<>();
	}
	
	public static Cart getInstance(){
		if(cartSingleton == null){
		cartSingleton = new Cart();
		}
		return cartSingleton;
	}

	public void addItem(String itemName){
		DBService dbService = new DBService();
		
		items.add(dbService.getItem(itemName));
	}
	
	public ArrayList<Item> getItems() {
		return items;
	}
	
	public void emptyCart(){
		items.clear();
	}
}
