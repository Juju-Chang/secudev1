package models;

import constants.ValidOperators;

public abstract class Filter{
	protected String isAnd;

 	public Filter(String isAnd){
		this.isAnd = isAnd;
	}

	@Override
	public String toString(){
		if(ValidOperators.withinAndOrOperators(isAnd)){
			return isAnd+" ";
		}
		return "";
	}

}