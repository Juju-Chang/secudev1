package models;

public class Item {
	private String name;
	private String description;
	private String imageSource;
	private int price;
	private int quantity;
	
	public Item(String name, String description, String imageSource, int price){
		this.name        = name;
		this.description = description;
		this.imageSource = imageSource;
		this.price       = price;
		this.quantity    = 0;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public String getImageSource() {
		return imageSource;
	}
	
	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}
	
	public int getQuantity(){
		return quantity;
	}
}
