package constants;

public enum SearchNames {
	QUERY, USERNAMES, 
	AND_ORS, DATE_CONDITIONS,
	DATE_YEARS, DATE_MONTHS, DATE_DAYS;
	
	public static SearchNames[] getAllExceptQuery(){
		return new SearchNames[]{USERNAMES, AND_ORS, DATE_CONDITIONS, DATE_YEARS, DATE_MONTHS, DATE_DAYS};
	}
	
	public String toString(){
		switch(this){
		case QUERY           : return "QUERY";
		case USERNAMES       : return "USERNAMES";
		case AND_ORS         : return "AND_ORS";
		case DATE_CONDITIONS : return "DATE_CONDITIONS";
		case DATE_YEARS      : return "DATE_YEARS";
		case DATE_MONTHS     : return "DATE_MONTHS";
		case DATE_DAYS       : return "DATE_DAYS";
		
		default              : return "invalid";
		}
	}
}
