package constants;

public enum Gender {
	FEMALE(1), MALE(0), INVALID(-1);
	
	private int opcode;
	
	private Gender(int opcode){
		this.opcode = opcode;
	}
	
	public int getOpcode(){
		return this.opcode;
	}
	
	public static Gender fromObject(Object value){
		if(value!=null){
			try{
				for(Gender g: Gender.values()){
					if(g.name().equalsIgnoreCase((String)value)){ 
						return g;
					}
				}
			}
			catch(Exception e){
				return INVALID;
			}
		}
		return INVALID;
	}
	
	@Override
	public String toString(){
		switch(this){
		case FEMALE : return "Female";
		case MALE   : return "Male";
		case INVALID: return "Invalid";
		}
		return "Invalid";
	}
}