package constants;

public enum DonateAmount {
	FIVE(5),
	TEN(10),
	TWENTY(20),
	INVALID(-1);
	
	private int value;
	
	private DonateAmount(int value){
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public static DonateAmount fromObject(Object value){
		if(value!=null){
			try{
				for(DonateAmount da: DonateAmount.values()){
					if(da.value == Integer.parseInt((String)value)){ 
						return da;
					}
				}
			}
			catch(Exception e){
				return INVALID;
			}
		}
		return INVALID;
	}
}
