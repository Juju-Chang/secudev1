package constants;

public enum ValidOperators {
	AND, OR, 
	EQUAL, GREATER_EQUAL, LESS_EQUAL;
	
	public String toString(){
		switch(this){
		case AND           : return "AND";
		case OR            : return "OR";
		
		case EQUAL         : return "=";
		case GREATER_EQUAL : return ">=";
		case LESS_EQUAL    : return "<=";
		default            : return "invalid";
		}
	}
	
	private static ValidOperators[] andOrOperators(){
		return new ValidOperators[]{AND, OR};
	}
	
	private static ValidOperators[] dateOperators(){
		return new ValidOperators[]{EQUAL, GREATER_EQUAL, LESS_EQUAL};
	}
	
	public static boolean withinAndOrOperators(String input){
		if(input!=null){
			for(ValidOperators v: andOrOperators()){
				if(input.equalsIgnoreCase(v.toString())){
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean withinDateOperators(String input){
		if(input!=null){
			for(ValidOperators v: dateOperators()){
				if(input.equals(v.toString())){
					return true;
				}
			}
		}
		return false;
	}	
}