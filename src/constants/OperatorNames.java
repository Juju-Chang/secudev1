package constants;

public enum OperatorNames {
	AND, OR;
	
	public String toString(){
		switch(this){
			case AND : return "AND";
			case OR  : return "OR";
			default  : return "invalid";
		}
	}
}
