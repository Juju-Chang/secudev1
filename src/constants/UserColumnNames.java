package constants;

public enum UserColumnNames {
	FIRST_NAME,  LAST_NAME, IS_FEMALE, 
	SALUTATION, BIRTHDATE, USERNAME, 
	PASSWORD, ABOUT_ME, IS_ADMIN,
	ENCRYPT, DATE_JOINED;

	public String toString(){
		switch(this){
		case FIRST_NAME  : return "fName";
		case LAST_NAME   : return "lName";
		case IS_FEMALE   : return "isFemale";
		case SALUTATION  : return "salutation";
		case BIRTHDATE   : return "birthDate";
		case USERNAME    : return "username";
		case PASSWORD    : return "PW";
		case ABOUT_ME    : return "aboutMe";
		case IS_ADMIN    : return "isAdmin";
		case ENCRYPT     : return "encrypt";
		case DATE_JOINED : return "dateJoined";
		default          : return "invalid";
		}
	}
}
