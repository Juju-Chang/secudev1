package constants;

public enum FieldNames {
	F_NAME, L_NAME, GENDER, SALUTATION,
	YEAR, MONTH, DAY,
	USERNAME, PASSWORD, CONFIRM_PASSWORD, 
	ABOUT_ME, IS_ADMIN;
	
	public String toString(){
		switch(this){
		case F_NAME           : return "fName";
		case L_NAME           : return "lName";
		case GENDER           : return "gender";
		case SALUTATION       : return "salutation";
		
		case YEAR             : return "year";
		case MONTH            : return "month";
		case DAY              : return "day";
		
		case USERNAME         : return "username";
		case PASSWORD         : return "password";
		case CONFIRM_PASSWORD : return "confirmPassword";
		case ABOUT_ME         : return "aboutMe";
		
		case IS_ADMIN         : return "isAdmin";
		default               : return "invalid";
		}
	}
	public static FieldNames[] getAllExceptUsername(){
		return new FieldNames[]{ F_NAME, L_NAME, GENDER, SALUTATION, YEAR, MONTH, DAY, 
								USERNAME, PASSWORD, CONFIRM_PASSWORD, ABOUT_ME, IS_ADMIN};
	}
}