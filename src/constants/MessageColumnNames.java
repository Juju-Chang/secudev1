package constants;

public enum MessageColumnNames {
	MESSAGE_ID, USERNAME, DATE_POSTED, DATE_EDITED, MESSAGE_BODY, FIRST_NAME;

	public static MessageColumnNames[] valuesExceptFName(){
		return new MessageColumnNames[]{MESSAGE_ID, USERNAME, DATE_POSTED, DATE_EDITED, MESSAGE_BODY};
	}
	public String toString(){
		switch(this){
		case MESSAGE_ID   : return "message_id";
		case USERNAME     : return "username";
		case DATE_POSTED  : return "datePosted";
		case DATE_EDITED  : return "dateEdited";
		case MESSAGE_BODY : return "body";
		case FIRST_NAME   : return "fname";
		default           : return "invalid";
		}
	}
}
