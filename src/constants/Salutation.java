package constants;

public enum Salutation {
	Mr, Sir, Senior, Count,
	Miss, Ms, Mrs, Majesty, Seniora,
	INVALID;
	
	private Gender gender;
	
	static{
		Mr.gender       = Gender.MALE;
		Sir.gender      = Gender.MALE;
		Senior.gender   = Gender.MALE;
		Count.gender    = Gender.MALE;

		Miss.gender     = Gender.FEMALE;
		Ms.gender       = Gender.FEMALE;
		Mrs.gender      = Gender.FEMALE;
		Majesty.gender  = Gender.FEMALE;
		Seniora.gender  = Gender.FEMALE;
		
		INVALID.gender  = Gender.INVALID;
	}

	public Gender getGender(){
		return gender;
	}
	
	public static Salutation fromObject(Object value){
		if(value!=null){
			try{
				for(Salutation s: Salutation.values()){
					if( s.name().equalsIgnoreCase((String)value) || 
						s.name().equalsIgnoreCase((String)value+".")){
						return s;
					}
				}
			}
			catch(Exception e){
				return INVALID;
			}
		}
		return INVALID;
	}
}