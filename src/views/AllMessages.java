package views;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import services.DBService;
import models.Message;

@WebServlet("/AllMessages/*")
public class AllMessages extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AllMessages() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String sPageNum  = request.getPathInfo() == null? "" : request.getPathInfo();
		
		if(sPageNum.length()<=1 || !sPageNum.startsWith("/")){
			out.println("Invalid Page Number");
		}
		else{
			try{
				int nPageNum = Integer.parseInt(sPageNum.substring(1));
				int n = 0;
				DBService db = new DBService();
				ArrayList<Message> m = new ArrayList<Message>();
				
				n = db.getNumOfPages();
				System.out.println(n);
				System.out.println("nPageNum is:"+nPageNum);
				m = db.get10Messages(nPageNum);
				
				HttpSession session = request.getSession();
				session.setAttribute("allMessages", m);
				session.setAttribute("numOfPages", n);
				
				RequestDispatcher rd = request.getRequestDispatcher("/messages.jsp");
				rd.forward(request, response);
			}
			catch(NumberFormatException e){
				out.println("Invalid Page Number");
				System.out.println(e.getMessage());
			}
		}
	}
}
