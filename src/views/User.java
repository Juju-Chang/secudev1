package views;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.DBService;

@WebServlet("/User/*")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public User() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String username = request.getPathInfo() == null? "" : request.getPathInfo();
		
		if(username.length()<=1 || !username.startsWith("/")){
			out.println("Invalid username");
		}
		else{
			DBService dbService = new DBService();
			models.User user    = dbService.getUserfromUsername(username.substring(1));
			
			if(user!=null){
				
				request.setAttribute("profile", user);
				request.setAttribute("profileMessages", dbService.getMessagesByUser(username.substring(1)));
				
				RequestDispatcher rd = request.getRequestDispatcher("/otherProfile.jsp");
				rd.forward(request, response);
			}
			else{
				System.out.println("Username "+username+" does not exist");
				out.println("Invalid username");
			}
		}
	}
}
