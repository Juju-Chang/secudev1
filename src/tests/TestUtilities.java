package tests;

import static net.sourceforge.jwebunit.junit.JWebUnit.assertRadioOptionSelected;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertSelectedOptionEquals;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertTextFieldEquals;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertTitleEquals;
import static net.sourceforge.jwebunit.junit.JWebUnit.beginAt;
import static net.sourceforge.jwebunit.junit.JWebUnit.clickRadioOption;
import static net.sourceforge.jwebunit.junit.JWebUnit.selectOptionByValue;
import static net.sourceforge.jwebunit.junit.JWebUnit.setBaseUrl;
import static net.sourceforge.jwebunit.junit.JWebUnit.setTextField;
import static net.sourceforge.jwebunit.junit.JWebUnit.submit;
import constants.FieldNames;
import constants.MessageColumnNames;

public class TestUtilities {
	protected final String textField         = "textField";
	protected final String select            = "select";
	protected final String checkBox          = "checkBox";
	
	protected final String registrationTitle = "Registration";
	protected final String loginTitle        = "Login";
	protected final String indexTitle        = "Index";
	protected final String messageTitle      = "Message Board";
	
	protected final String loginLink         = "login.jsp";
	protected final String registrationLink  = "registration.jsp";
	protected final String messageLink  	 = "AllMessages/0";
	
	protected void setupPrepare(){
		setBaseUrl("http://localhost:8080/Secudev1/");
	}
	
	protected void prepare(String begin, String title){
		setBaseUrl("http://localhost:8080/Secudev1/");
		beginAt(begin); 
        assertTitleEquals(title);
	}
	
	protected void login(){
        prepare(loginLink, loginTitle);
        setTextField("username", "Doctor99");
        setTextField("password", "whooster");
        submitAssertTitle(indexTitle);
	}
	
	protected void setAndCheck(String type, MessageColumnNames m, String value){
		setAndCheck(type, m.toString(), value);
	}
	protected void setAndCheck(String type, FieldNames f, String value){
		setAndCheck(type, f.toString(), value);
	}
    protected void setAndCheck(String type, String fieldName, String value){
    	switch(type){
    	case textField:  
    		setTextField(fieldName, value);
    		assertTextFieldEquals(fieldName, value);
    		break;
    	case select:
    		selectOptionByValue(fieldName, value);
    		assertSelectedOptionEquals(fieldName, value);
    		break;
    	case checkBox:
    		clickRadioOption(fieldName, value);
    		assertRadioOptionSelected(fieldName, value);
    		break;
    	}
    }
    
    protected void submitAssertTitle(String title){
        submit();
        assertTitleEquals(title);    	
    }
}
