package tests;

import static net.sourceforge.jwebunit.junit.JWebUnit.*;

import org.junit.Before;
import org.junit.Test;

public class LoginTest extends TestUtilities{
	private final String usernameField = "username";
	private final String passwordField = "password";
	
    @Before
    public void prepare() {
        super.prepare(loginLink, loginTitle);
    }

    @Test
    public void validLogin() {
        setTextField(usernameField, "Username");
        setTextField(passwordField, "Username");
        submitAssertTitle(indexTitle);
    }
    
    @Test
    public void invalidLogin() {
      setTextField(usernameField, "invalid");
      setTextField(passwordField, "invalid");
      submitAssertTitle(loginTitle);
    }
    
    @Test
    public void nullValues(){
    	submitAssertTitle(loginTitle);
    }

}
