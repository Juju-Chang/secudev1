package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import services.TestDBService;
import constants.FieldNames;
import constants.Gender;
import constants.Salutation;

public class RegisterTest extends TestUtilities{
	private final String username          = "test";
	
    @Before
    public void prepare() {
    	super.prepare(registrationLink, registrationTitle);
    }

    @Test
    public void validRegistration() {
    	setValidFields();
    	
		submitAssertTitle(loginTitle);
    }
    
    @Test
    public void invalidName(){
    	setValidFields();
    	setAndCheck(textField, FieldNames.F_NAME, "@D_M@!0R3M");
    	setAndCheck(textField, FieldNames.L_NAME, "@D_M@!0R3M");
    	submitAssertTitle(registrationTitle);
    }
    
    @Test
    public void nullFields(){
    	submitAssertTitle(registrationTitle);
    }
    
    @After
    public void removeAccount(){
    	TestDBService testDB = new TestDBService();
    	testDB.deleteUser(username);
    }
    
    private void setValidFields(){
		setAndCheck(textField, FieldNames.F_NAME           , "una");
		setAndCheck(textField, FieldNames.L_NAME           , "huli");

		setAndCheck(checkBox,  FieldNames.GENDER           , Gender.MALE.toString());
		setAndCheck(select,    FieldNames.YEAR             , "1901");
		setAndCheck(select,    FieldNames.MONTH            , "1");
		setAndCheck(select,    FieldNames.DAY              , "12");
		
		setAndCheck(select,    FieldNames.SALUTATION       , Salutation.Mr.toString());
		setAndCheck(textField, FieldNames.USERNAME         , username);
		setAndCheck(textField, FieldNames.PASSWORD         , "test");
		setAndCheck(textField, FieldNames.CONFIRM_PASSWORD , "test");
		setAndCheck(textField, FieldNames.ABOUT_ME         , "I'm a test");    	
    }
}
